(function () {
    _init = function(){
        bindUI();
    };
    bindUI = function() {
        $(document).on("click", ".saveUser", addNewUser);
        $(document).on("click", ".viewAllUser", viewAllUsers);
    };
    
    addNewUser = (event) => {
        let $form = $("#addNewUserForm");
        let formData = new FormData($form[0]);
        if($form .valid()) {
            $.ajax({
                method: "POST",
                url: "/addUser",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    console.log(response);
                },
                error: (error) => {
                    console.log("Error in User add", error);
                }

            });
        }
    };
    viewAllUsers = (event) => {
        $(".viewUser").remove();
        $.ajax({
            method: "GET",
            url: "/viewUsers",
            success: (response) => {
                $(".js-main").append(response);
                $(".viewUser").fadeIn(500);
            },
            error: (error) => {
                console.log("Error in User add", error);
            }

        });
    }
}());
_init();