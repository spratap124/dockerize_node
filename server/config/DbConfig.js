module.exports = {
	opts: {
        url: 'mongodb://mongodb:27017/dockerUsers',
        settings: {
            poolSize: 10
        },
        decorate: true
    }
}