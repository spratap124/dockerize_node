"use strict";

const getAllUsers = async (request, reply) => {
    const db = request.mongo.db;
    const ObjectID = request.mongo.ObjectID;
    let context = {};
    try {
        const userColl = db.collection('users');
        const allUsers =  await userColl.find().toArray();
        context.users = allUsers;
        console.log("context === ", allUsers);
        return reply.view("allUsers", context);
    }
    catch (err) {
        return `Error fetching users ${err}`;
    }
}

module.exports = getAllUsers;