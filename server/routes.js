"use strict";
const rootHandler = require("./handlers/rootHandler");
const addNewUserHandler = require("./handlers/addNewUserHandler");
const getAllUsers = require("./handlers/getAllUsers");

const routes = [
  {// serve css files
      method: 'GET',
      path: '/css/{file*}',
      handler: {
          directory: {
              path: 'client/css',
              listing: true
          }
      }
  },
  {// serve js files
      method: 'GET',
      path: '/js/{file*}',
      handler: {
          directory: {
              path: 'client/js',
              listing: true
          }
      }
  },
  {
    method: "GET",
    path: "/",
    handler: rootHandler
  },
  {
    method: "POST",
    path: "/addUser",
    handler: addNewUserHandler
  },
  {
    method: "GET",
    path: "/viewUsers",
    handler: getAllUsers
  }
];

module.exports = routes;
